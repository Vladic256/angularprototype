@Directive({
  selector: '[bsModal]',
  exportAs: 'bs-modal'
})
export class ModalDirective implements AfterViewInit, OnDestroy {
  @Input()
  public set config(conf:ModalOptions) {
    this._config = this.getConfig(conf);
  };

  @Output() public onShow:EventEmitter<ModalDirective> = new EventEmitter();
  @Output() public onShown:EventEmitter<ModalDirective> = new EventEmitter();
  @Output() public onHide:EventEmitter<ModalDirective> = new EventEmitter();
  @Output() public onHidden:EventEmitter<ModalDirective> = new EventEmitter();

}
