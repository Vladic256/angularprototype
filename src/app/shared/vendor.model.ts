export class Vendor {
  public key: string;
  public label: string;

  constructor(value: string, label: string) {
    this.key = value;
    this.label = label;
  }
}
