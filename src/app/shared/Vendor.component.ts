import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'vendor-dropdown',
  inputs: ['DropValues'],
  outputs: ['select'],
  template: `
    <select [(ngModel)]="objValue" (change)="selectItem($event.target.value)"> // value is an object
        <option *ngFor="#obj of DropValues" (click) = "selectItem(obj)"  [value]="obj.label">{{obj.label}}</option>
    </select>
    `
})

export class DropDown {
  public select: EventEmitter = new EventEmitter();
  constructor() {
  }

  selectItem(value) {
      this.select.next(value);
  }
}
