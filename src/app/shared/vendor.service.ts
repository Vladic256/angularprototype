import { Injectable } from '@angular/core';
import { Vendor } from './vendor.model';

@Injectable()
export class VendorService {
  private vendorlist = [];

  constructor() {
    this.vendorlist.push(new Vendor("1", "Apple"));
    this.vendorlist.push(new Vendor("2", "Microsoft"));
    this.vendorlist.push(new Vendor("3", "Google"));
  }
  getAllVendor() {
    return this.vendorlist;
  }
}
