import { Injectable } from '@angular/core';
import { ProductModel } from './product.model';

@Injectable()
export class ProductService {

  private productsList = [];

  constructor() {

    this.productsList.push(new ProductModel(1, 'LipStick', 23, 3, "vendor1", ""));
    this.productsList.push(new ProductModel(2, 'Iphone Case', 234, 34, "Google", ""));
    this.productsList.push(new ProductModel(3, 'Car Seat', 14, 45, "Apple", ""));
    this.productsList.push(new ProductModel(4, 'Coffee', 89, 23, "Microsoft", ""));
  }

  //Update a specific product
  public updateProduct(id: number): void {

  }

  //Retrieve all the products available.
  public getAllProducts() {

    return this.productsList;
  }

  //Create new product
  public createProduct(name: string, cost: number, price: number, vendor: string) {

  }
}
