import { Component, EventEmitter, ViewChild, OnInit } from '@angular/core';
import { ProductModel } from './product.model';

import { MODAL_DIRECTIVES, BS_VIEW_PROVIDERS } from 'ng2-bootstrap/ng2-bootstrap';
import {CORE_DIRECTIVES } from '@angular/common';
import { VendorService } from '../shared/vendor.service';
import { DropDown } from '../shared/Vendor.component';
import { Vendor } from '../shared/vendor.model';

@Component({
  selector: 'product',
  inputs: ['productInfo'],
  directives: [MODAL_DIRECTIVES, CORE_DIRECTIVES, DropDown],
  viewProviders: [BS_VIEW_PROVIDERS],
  providers: [VendorService],
  outputs: ['deleteEvent', 'edit'],
  styles: [`
      md-card {
        box-shadow: 0px 2px 1px -1px rgba(0, 0, 0, 0.2), 0px 1px 1px 0px rgba(0, 0, 0, 0.14), 0px 1px 3px 0px rgba(0, 0, 0, 0.12);
        -webkit-transition: box-shadow 280ms cubic-bezier(0.4, 0, 0.2, 1);
        transition: box-shadow 280ms cubic-bezier(0.4, 0, 0.2, 1);
        will-change: box-shadow;
        display: block;
        position: relative;
        padding: 24px;
        border-radius: 2px;
        font-family: Roboto, "Helvetica Neue", sans-serif;
        background: white;
        color: black; }

      md-card:hover {
        box-shadow: 0px 3px 1px -2px rgba(0, 0, 0, 0.2), 0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 1px 5px 0px rgba(0, 0, 0, 0.12); }

      .md-card-flat {
        box-shadow: none; }

      md-card-title, md-card-subtitle, md-card-content, md-card-actions {
        display: block;
        margin-bottom: 16px; }

      md-card-title {
        font-size: 24px;
        font-weight: 400; }

      md-card-subtitle {
        font-size: 14px;
        color: rgba(0, 0, 0, 0.54); }

      md-card-content {
        font-size: 14px; }

      md-card-actions {
        margin-left: -16px;
        margin-right: -16px;
        padding: 8px 0; }
        md-card-actions[align='end'] {
          display: -webkit-box;
          display: -ms-flexbox;
          display: flex;
          -webkit-box-pack: end;
              -ms-flex-pack: end;
                  justify-content: flex-end; }

      [md-card-image] {
        width: calc(100% + 48px);
        margin: 0 -24px 16px -24px; }

      [md-card-xl-image] {
        width: 240px;
        height: 240px;
        margin: -8px; }

      md-card-footer {
        position: absolute;
        bottom: 0; }

      md-card-actions [md-button], md-card-actions [md-raised-button] {
        margin: 0 4px; }

      /* HEADER STYLES */
      md-card-header {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
            -ms-flex-direction: row;
                flex-direction: row;
        height: 40px;
        margin: -8px 0 16px 0; }

      .md-card-header-text {
        height: 40px;
        margin: 0 8px; }

      [md-card-avatar] {
        height: 40px;
        width: 40px;
        border-radius: 50%; }

      md-card-header md-card-title {
        font-size: 14px; }

      /* TITLE-GROUP STYLES */
      [md-card-sm-image], [md-card-md-image], [md-card-lg-image] {
        margin: -8px 0; }

      md-card-title-group {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: justify;
            -ms-flex-pack: justify;
                justify-content: space-between;
        margin: 0 -8px; }

      [md-card-sm-image] {
        width: 80px;
        height: 80px; }

      [md-card-md-image] {
        width: 112px;
        height: 112px; }

      [md-card-lg-image] {
        width: 152px;
        height: 152px; }

      /* MEDIA QUERIES */
      @media (max-width: 600px) {
        md-card {
          padding: 24px 16px; }
        [md-card-image] {
          width: calc(100% + 32px);
          margin: 16px -16px; }
        md-card-title-group {
          margin: 0; }
        [md-card-xl-image] {
          margin-left: 0;
          margin-right: 0; }
        md-card-header {
          margin: -8px 0 0 0; } }

      /* FIRST/LAST CHILD ADJUSTMENTS */
      .md-card > :first-child, md-card-content > :first-child {
        margin-top: 0; }

      .md-card > :last-child, md-card-content > :last-child {
        margin-bottom: 0; }

      [md-card-image]:first-child {
        margin-top: -24px; }

      .md-card > md-card-actions:last-child {
        margin-bottom: -16px;
        padding-bottom: 0; }

      md-card-actions [md-button]:first-child,
      md-card-actions [md-raised-button]:first-child {
        margin-left: 0;
        margin-right: 0; }

      md-card-title:not(:first-child), md-card-subtitle:not(:first-child) {
        margin-top: -4px; }

      md-card-header md-card-subtitle:not(:first-child) {
        margin-top: -8px; }

      .md-card > [md-card-xl-image]:first-child {
        margin-top: -8px; }

      .md-card > [md-card-xl-image]:last-child {
        margin-bottom: -8px; }

        .container {
            width: 40%;
        }
`],
  template: `
  <div class="container">
  <md-card>
    <md-card-header>
      <img md-card-avatar src="https://material.angularjs.org/latest/img/washedout.png">
      <md-card-title>{{productInfo.Name}}</md-card-title>
      <md-card-subtitle>{{productInfo.Description}}</md-card-subtitle>
    </md-card-header>
    <img md-card-image alt="Main image" src="https://material.angularjs.org/latest/img/washedout.png">
    <md-card-content>
      <md-card-title>{{productInfo.Vendor}}</md-card-title>
      <p>Cost: &nbsp;{{productInfo.Cost | currency:'USD':true:'3.2-2' }}</p>
      <p>Price: &nbsp; {{productInfo.Price | currency:'USD':true:'3.2-2' }}</p>
      <md-button class="md-raised md-primary" (click) = "DeleteProduct()"> Delete </md-button>
      <md-button class="md-raised md-primary" (click)="openEdit()"> Edit </md-button>

     <div *ngIf="edit==true">
     <md-input-container>
       <label>Name</label>
       <input [(ngModel)]="productInfo.Name">
     </md-input-container>
     <md-input-container>
       <label>Cost</label>
       <input type="number" [(ngModel)]="productInfo.Cost">
     </md-input-container>
     <md-input-container>
       <label>Price</label>
       <input [(ngModel)]="productInfo.Price">
     </md-input-container>

       <vendor-dropdown [DropValues] ="DropValues" (select)="getVendorName($event)"></vendor-dropdown>


     <md-button (click)="saveProduct()"> Save</md-button>
     </div>
    </md-card-content>
  </md-card>


`

})

export class Product {

  public productInfo: ProductModel;
  DropValues: Vendor[];
  edit: boolean;
  public deleteEvent: EventEmitter = new EventEmitter();

  @ViewChild('smModal') public childModal: MODAL_DIRECTIVES;

  constructor(private vendorservice: VendorService) {

  }

  public showChildModal(): void {
    console.log("testings");
    this.childModal.show();
  }
  openEdit() {
    this.edit = !this.edit;
  }
  public hideChildModal(): void {
    this.childModal.hide();
  }

   ngOnInit() {
       this.DropValues = this.vendorservice.getAllVendor();
   }

   getVendorName (arg){
       this.productInfo.Vendor = arg;
   }

  saveProduct() {
    this.edit = false;
  }
  //Fire the delete function.
  DeleteProduct(evt) {
    this.deleteEvent.next(this.productInfo);
  }
}
