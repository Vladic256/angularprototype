import { Component, OnInit } from '@angular/core';
import { ProductService } from './product.service';
import { VendorService } from '../shared/vendor.service';
import { Product } from './product-component';
import { ProductModel } from './product.model';
import { DropDown } from '../shared/Vendor.component';
import { Vendor } from '../shared/vendor.model';

@Component({
  selector: 'products-listing',
  templateUrl: './product.listin.template.html',
  styleUrls: ['listin.css'],
  directives: [Product, DropDown],
  providers: [ProductService, VendorService]
})

export class ProductListing {
  products: ProductModel[];
  vendorList: Vendor[];
  DropValues: Vendor[];
  showAdd: boolean;
  newProduct: ProductModel;
  cost: number;
  productDetails = {
    name: '',
    cost: 0,
    price: 0,
    vendor: '',
    url: ''
  }

  constructor(private service: ProductService, private vendorservice: VendorService) {

  }
  toggleAddProduct() {

    this.showAdd = !this.showAdd;
  }

  AddNewProduct() {
    console.log(this.cost);
    var j = new ProductModel(this.products.length +1 , this.productDetails.name, this.productDetails.cost, this.productDetails.price, this.productDetails.vendor, "")
    this.products.push(j)
    this.showAdd = false;
  }
  select(value) { }

  ngOnInit() {
    this.products = this.service.getAllProducts();
    this.DropValues = this.vendorservice.getAllVendor();
    console.log(this.DropValues);
  }

  onSubmit(form: any): void {
    console.log('you submitted value:', form);
  }

   getVendorName (arg){
       this.productDetails.vendor = arg;
       console.log(arg);
   }

  deleteProductEvent(arg) {
    console.log(arg);
    var index = this.products.indexOf(arg);
    if (index > -1) {
      this.products.splice(index, 1);
      console.log(index);
    }
  }
}
