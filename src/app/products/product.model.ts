//Model class for a product
export class ProductModel {

  public Id: number;
  public Name: string;
  public Cost: number;
  public Price: number;
  public Vendor: string;
  public ImageUrl: string;

  constructor(id: number, name: string, cost: number, price: number, vendor: string, imageurl: string) {
      console.log(cost);
    this.Id = id;
    this.Name = name;
    this.Cost = cost;
    this.Price = price;
    this.Vendor = vendor;
    this.ImageUrl = imageurl;
  }

}
